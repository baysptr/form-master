import React, { Component } from 'react';
import axios from 'axios';
import Navigation from './components/Navigation';
import { Modal, ModalHeader, ModalBody } from 'reactstrap';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      action: 'add',
      id: '',
      kd_barang: '',
      nm_barang: '',
      satuan: '',
      harga: '',
      barangs: []
    };

    this.toggle = this.toggle.bind(this);
    this.handleChangeId = this.handleChangeId.bind(this);
    this.handleChangeKd = this.handleChangeKd.bind(this);
    this.handleChangeNd = this.handleChangeNd.bind(this);
    this.handleChangeSat = this.handleChangeSat.bind(this);
    this.handleChangeHar = this.handleChangeHar.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleEditData = this.handleEditData.bind(this);
    this.handleDeleteData = this.handleDeleteData.bind(this);
  }

  handleDeleteData(id){
    if(window.confirm('Apakah Anda Yakin akan menghapus data ini ?') === true){
      axios.get('http://localhost/form_request/delete.php?id_barang='+id)
      .then(res => {
        if(res.data.status === true){
          this.componentDidMount();
        }else{
          alert('Maaf data anda belum bisa kami proses!');
          this.componentDidMount();
        }
      })
    }
  }

  handleEditData(id){
    axios.get('http://localhost/form_request/get_where.php?id_barang='+id)
    .then(res => {
      console.log(res.data);
      const tempData = res.data;
      this.setState({
        action: 'update',
        id: tempData[0],
        kd_barang: tempData[1],
        nm_barang: tempData[2],
        satuan: tempData[3],
        harga: tempData[4],
      });
      this.toggle();
    })
  }

  componentDidMount(){
    axios.get('http://localhost/form_request/form_view.php')
      .then(res => {
        const barangs = res.data.barang;
        this.setState({barangs: barangs});
        console.log(this.state.barangs);
      })
  }

  handleSubmit(e){
    e.preventDefault();
    // console.log(this.state);

    const headers = {
      'Content-Type': 'application/x-www-form-urlencoded',
    }

    axios.post(`http://localhost/form_request/form_save.php`, {
      action: this.state.action,
      id_barang: this.state.id,
      kd_barang: this.state.kd_barang,
      nm_barang: this.state.nm_barang,
      satuan: this.state.satuan,
      harga: this.state.harga
    }, {headers: headers})
      .then(res => {
        if(res.data.status === true){
          this.setState({
            action: 'add',
            id: '',
            kd_barang: '',
            nm_barang: '',
            satuan: '',
            harga: '',
          });
          this.toggle();
          this.componentDidMount();
        }else{
          alert('Maaf data anda belum bisa kami proses!');
          this.toggle();
        }
      })
  }

  handleChangeId(e){
    this.setState({ id: e.target.value });
  }

  handleChangeKd(e){
    this.setState({ kd_barang: e.target.value });
  }

  handleChangeNd(e){
    this.setState({ nm_barang: e.target.value });
  }

  handleChangeSat(e){
    this.setState({ satuan: e.target.value });
  }

  handleChangeHar(e){
    this.setState({ harga: e.target.value });
  }

  toggle() {
    this.setState({
      modal: !this.state.modal
    });
  }


  render() {
    return (
      <div>
        <Navigation/>
        <div className='container'>
          <div className='jumbotron'>
            <center><h1>Master Barang</h1></center><br/>
            <form>
              <table className='table table-striped table-bordered'>
                <thead className='table-light'>
                  <tr>
                    <td>Kode Barang</td>
                    <td>Nama Barang</td>
                    <td>Satuan</td>
                    <td>Harga</td>
                    <td colSpan='2'>
                      <div onClick={this.toggle} className='btn btn-sm btn-block btn-primary'><span className='fa fa-plus'></span></div>
                    </td>
                  </tr>
                </thead>
                <tbody>
                  { this.state.barangs.map(brng => 
                    <tr>
                      <td>{brng.kd_barang}</td>
                      <td>{brng.nm_barang}</td>
                      <td>{brng.satuan}</td>
                      <td>{brng.harga}</td>
                      <td>
                        <div onClick={() => this.handleDeleteData(brng.id_barang)} className='btn btn-sm btn-block btn-danger'><span className='fa fa-trash'></span></div>
                      </td>
                      <td>
                        <div onClick={() => this.handleEditData(brng.id_barang)} className='btn btn-sm btn-block btn-info'><span className='fa fa-edit'></span></div>
                      </td>
                    </tr>
                    ) }
                </tbody>
              </table>
            </form>
          </div>
        </div>

        <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
          <ModalHeader toggle={this.toggle}>Form Tambah Barang</ModalHeader>
          <ModalBody>
            <form onSubmit={this.handleSubmit}>
              <input type='hidden' value={this.state.id}/>
              <table className='table table-striped table-bordered'>
                <tr>
                  <td>Kode Barang</td>
                  <td><input type='text' value={this.state.kd_barang} onChange={this.handleChangeKd} className='form-control'></input></td>
                </tr>
                <tr>
                  <td>Nama Barang</td>
                  <td><input type='text' value={this.state.nm_barang} onChange={this.handleChangeNd} className='form-control'></input></td>
                </tr>
                <tr>
                  <td>Satuan</td>
                  <td><input type='text' value={this.state.satuan} onChange={this.handleChangeSat} className='form-control'></input></td>
                </tr>
                <tr>
                  <td>Harga</td>
                  <td><input type='text' value={this.state.harga} onChange={this.handleChangeHar} className='form-control'></input></td>
                </tr>
                <tr>
                  <td></td>
                  <td><button type='submit' className='btn btn-sm btn-primary'>Save</button></td>
                </tr>
              </table>
            </form>
          </ModalBody>
        </Modal>

      </div>
    );
  }
}

export default App;
