import React from 'react';

class Navigation extends React.Component{
    render(){
        return(
            <nav class="navbar navbar-expand-md navbar-dark bg-dark mb-4">
                <a class="navbar-brand" href="#">Form Master</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Master Barang</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Master xxx</a>
                        </li>
                    </ul>
                </div>
            </nav>
        )
    }
}

export default Navigation;